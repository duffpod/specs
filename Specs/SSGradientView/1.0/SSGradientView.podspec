#
# Be sure to run `pod spec lint SSGradientView.podspec' to ensure this is a
# valid spec and remove all comments before submitting the spec.
#
# To learn more about the attributes see http://docs.cocoapods.org/specification.html
#
Pod::Spec.new do |s|
  s.name         = "SSGradientView"
  s.version      = "1.0"
  s.summary      = "Easy gradient view"
  s.homepage     = "https://github.com/samsoffes/sstoolkit"
  s.author       = { "Sam Soffes" => "http://github.com/soffes" }
  s.license      = "MIT"

  s.source       = { :git => "git@bitbucket.org:duffpod/ssgradientview.git", :tag => s.version }

  s.ios.deployment_target = '5.0'
  s.osx.deployment_target = '10.7'

  s.requires_arc = true

  s.source_files = 'Public', 'Public/*.{h,m}'

end
